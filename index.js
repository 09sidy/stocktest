const electron = require('electron')

const app = electron.app
const BrowserWindow = electron.BrowserWindow

var mainWindow = null

function createWindow(){
	var windowOptions = {
      width: 800,
      minWidth: 600,
      height: 600,
  }

  mainWindow = new BrowserWindow(windowOptions)
  //mainWindow.loadURL(path.join('file://', __dirname, '/index.html'))
  mainWindow.loadURL('file://' + __dirname + '/index.html')
  //mainWindow.loadURL('index.html')

}

app.on('ready', function () {
    createWindow()
  })

app.on('window-all-closed', function () {
    if (process.platform !== 'darwin') {
      app.quit()
    }
  })